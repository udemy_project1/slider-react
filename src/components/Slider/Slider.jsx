import React, { useEffect, useState } from "react";
import chevron from "../../assets/next-95.svg";
import "./slider.css";
import sliderData from "../../data/sliderData";

const Slider = () => {
	const [sliderIndex, setSliderIndex] = useState(2);

	const toggleImage = (indexPayload) => {
		// let newState;
		// if (indexPayload + sliderIndex > sliderData.length) {
		// 	newState = 1;
		// } else if (indexPayload + sliderIndex < 1) {
		// 	newState = sliderData.length;
		// } else {
		// 	newState = indexPayload + sliderIndex;
		// }
		// setSliderIndex(newState);

		setSliderIndex((state) => {
			if (indexPayload + state > sliderData.length) return 1;
			else if (indexPayload + state < 1) return sliderData.length;
			else return indexPayload + state;
		});
	};
	useEffect(() => {
		const intervalID = setInterval(() => toggleImage(1), 2000);
		return () => clearInterval(intervalID);
	}, []);
	return (
		<div className="bill__slider">
			<p className="bill__slider-index">
				{sliderIndex}/{sliderData.length}
			</p>
			<div className="bill__slider-container">
				<p className="bill__slider-container_info">{sliderData.find((obj) => obj.id === sliderIndex).description}</p>
				<img
					src={`images/pub${sliderIndex}.png`}
					alt="first image"
					className="bill__slider-container_img"
				/>
				<button
					onClick={() => toggleImage(-1)}
					className="bill__slider-container_navigation prev-button">
					<img
						src={chevron}
						alt="previous image"
					/>
				</button>
				<button
					onClick={() => toggleImage(1)}
					className="bill__slider-container_navigation next-button">
					<img
						src={chevron}
						alt="next image"
					/>
				</button>
			</div>
		</div>
	);
};

export default Slider;
